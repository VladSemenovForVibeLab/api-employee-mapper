# Учет Employee

## Описание
Учет Employee - это приложение, разработанное на Spring Boot, которое предоставляет функциональность для управления данными сотрудников.

## Требования
- Java 17
- Spring Boot 3.1.5
- Spring Boot Starter Web
- MariaDB
- Lombok
- Spring Data JPA
- Hibernate

## Установка и запуск
1. Сначала убедитесь, что у вас установлены Java 17 и MariaDB.
2. Клонируйте репозиторий с помощью команды `git clone git@gitlab.com:VladSemenovForVibeLab/api-employee-mapper.git`.
3. Перейдите в каталог проекта с помощью команды `cd app`.
4. Откройте файл `application.properties` и настройте подключение к базе данных MariaDB.
5. Запустите приложение с помощью команды `./mvnw spring-boot:run`.

## Использование
После успешного запуска приложение будет доступно по адресу `http://localhost:8080`.

Доступные эндпоинты:

- `GET /employees` - получить список всех сотрудников
- `GET /employees/{id}` - получить информацию о сотруднике по его идентификатору
- `POST /employees` - создать нового сотрудника
- `PUT /employees/{id}` - изменить информацию о сотруднике по его идентификатору
- `DELETE /employees/{id}` - удалить сотрудника по его идентификатору

## Примеры запросов

- Запрос GET `/employees`:
  ```
  GET http://localhost:8080/employees
  ```

- Запрос GET `/employees/{id}`:
  ```
  GET http://localhost:8080/api/employees/1
  ```

- Запрос POST `/employees`:
  ```
  POST http://localhost:8080/api/employees
  
  {
    "firstName":"{{$randomFirstName}}",
    "lastName":"{{$randomLastName}}",
    "email":"{{$randomEmail}}"
  }
  ```

- Запрос PUT `/employees/{id}`:
  ```
  PUT http://localhost:8080/api/employees/1
  
  {
    "id": 1,
    "firstName": "Lauriane123123213",
    "lastName": "Schmeler",
    "email": "Jeramy_Schiller@yahoo.com"
  }
  ```

- Запрос DELETE `/employees/{id}`:
  ```
  DELETE http://localhost:8080/api/employees/1
  ```