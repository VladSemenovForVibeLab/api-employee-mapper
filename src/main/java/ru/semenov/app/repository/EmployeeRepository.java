package ru.semenov.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.app.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
